package ro.cmocanu.drools.utils;

import org.kie.api.definition.rule.Rule;
import org.kie.api.event.rule.AfterMatchFiredEvent;
import org.kie.api.event.rule.DefaultAgendaEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class RulesTracker extends DefaultAgendaEventListener {

    private static Logger LOG = LoggerFactory.getLogger(RulesTracker.class);

    @Override
    public void afterMatchFired(AfterMatchFiredEvent event) {
        super.afterMatchFired(event);
        Rule rule = event.getMatch().getRule();

        LOG.debug("Match Rule '{}', namespace '{}'", rule.getName(), rule.getNamespace());
    }
}
