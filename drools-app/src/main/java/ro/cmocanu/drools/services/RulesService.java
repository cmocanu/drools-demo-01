package ro.cmocanu.drools.services;

import org.kie.api.KieBase;
import org.kie.api.event.rule.AgendaEventListener;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieContainerSessionsPool;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.StatelessKieSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ro.cmocanu.drools.objects.Transaction;
import ro.cmocanu.drools.utils.RulesTracker;

@Service
public class RulesService {

    private final KieContainer kieContainer;
    private KieContainerSessionsPool kieSessionPool;

    private final static Logger LOG = LoggerFactory.getLogger(RulesService.class);

    @Autowired
    RulesTracker rulesTracker;

    @Value("${drools.session.name}")
    private String sessionName;
    @Value("${drools.session.initial_pool_size}")
    private int initialPoolSize;

    @Autowired
    public RulesService(KieContainer kieContainer) {
        LOG.info("Initialising a new RulesService session.");
        this.kieContainer = kieContainer;
    }

    public KieSession kieSession() {
        KieSession kieSession = null;
        if (this.kieSessionPool == null) {
            this.kieSessionPool = kieContainer.newKieSessionsPool(initialPoolSize);
        }

        kieSession = this.kieSessionPool.newKieSession(sessionName);
        if(LOG.isDebugEnabled()) {
            kieSession.addEventListener(rulesTracker);
        }

        return kieSession;
    }

    public Transaction evaluateTransaction(Transaction txn) {
        LOG.debug("Input object: {}", txn);
        KieSession kieSession = kieSession();
        kieSession.insert(txn);
        kieSession.fireAllRules();
        kieSession.dispose();
        LOG.debug("Output object: {}", txn);

        return txn;
    }

    public void shutdown() {
        if(this.kieSessionPool != null) {
            this.kieSessionPool.shutdown();
            this.kieSessionPool = null;
        }
        this.kieContainer.dispose();
    }
}
