package ro.cmocanu.drools;

import org.kie.api.KieServices;
import org.kie.api.builder.ReleaseId;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import ro.cmocanu.drools.objects.Transaction;
import ro.cmocanu.drools.services.RulesService;

import javax.annotation.PreDestroy;
import java.math.BigDecimal;

@SpringBootApplication
public class App implements CommandLineRunner {

    private static final Logger LOG = LoggerFactory.getLogger(App.class);

    @Autowired
    private RulesService rulesService;

    public static void main(String args[]) {

        SpringApplication.run(App.class, args);

    }

    @Bean
    public KieContainer kieContainer() {
        return KieServices.Factory.get().getKieClasspathContainer();
    }

    @PreDestroy
    public void shutdown() {
        LOG.info("Close KieSession pool");
        rulesService.shutdown();
    }

    @Override
    public void run(String... args) {
        LOG.info("EXECUTING : command line runner");

        Transaction txn10 = new Transaction();
        txn10.setTxnId(1L);
        txn10.setChannel("EFT");
        txn10.setFromAccount(1L);
        txn10.setToAccount(10L);
        txn10.setAmount(BigDecimal.valueOf(1000));

        Transaction txn11 = new Transaction();
        txn11.setTxnId(1L);
        txn11.setChannel("EFT");
        txn11.setFromAccount(1L);
        txn11.setToAccount(10L);
        txn11.setAmount(BigDecimal.valueOf(100000));

        Transaction txn12 = new Transaction();
        txn12.setTxnId(1L);
        txn12.setChannel("EFT");
        txn12.setFromAccount(1L);
        txn12.setToAccount(10L);
        txn12.setAmount(BigDecimal.valueOf(10000));
        txn12.setMcc(Long.valueOf(3641));

        Transaction txn20 = new Transaction();
        txn20.setChannel("IBK");
        txn20.setTxnId(20L);
        txn20.setFromAccount(1L);
        txn20.setToAccount(10L);
        txn20.setAmount(BigDecimal.valueOf(100000));
        txn20.setDescription1("Salariu martie");

        Transaction txn21 = new Transaction();
        txn21.setChannel("IBK");
        txn21.setTxnId(21L);
        txn21.setFromAccount(1L);
        txn21.setToAccount(10L);
        txn21.setAmount(BigDecimal.valueOf(1000));
        txn21.setDescription1("Alocatie copil");

        Transaction txn22 = new Transaction();
        txn22.setChannel("IBK");
        txn22.setTxnId(22L);
        txn22.setFromAccount(1L);
        txn22.setToAccount(10L);
        txn22.setAmount(BigDecimal.valueOf(1000));
        txn22.setDescription1("Cadou amanta");

        rulesService.evaluateTransaction(txn10);
        rulesService.evaluateTransaction(txn11);
        rulesService.evaluateTransaction(txn12);
        rulesService.evaluateTransaction(txn20);
        rulesService.evaluateTransaction(txn21);
        rulesService.evaluateTransaction(txn22);

    }
}
